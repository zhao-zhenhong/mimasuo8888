/***********************************************
/gen auto by zuitools
***********************************************/
#include "mainActivity.h"

/*TAG:GlobalVariable全局变量*/
static ZKButton* mButtonNextPtr;
static ZKButton* mButtonPrevPtr;
static ZKTextView* mTextViewDevNamePtr;
static ZKButton* mButtonChange3Ptr;
static ZKTextView* mTextViewChange2Ptr;
static ZKTextView* mTextViewChange1Ptr;
static ZKTextView* mTextViewEnter1Ptr;
static ZKButton* mButtonshezhiPtr;
static ZKButton* mButtonsuobiaoPtr;
static ZKTextView* mTextViewWFPtr;
static ZKTextView* mTextViewWEPtr;
static ZKTextView* mTextViewWBPtr;
static ZKTextView* mTextViewWAPtr;
static ZKTextView* mTextViewW9Ptr;
static ZKTextView* mTextViewW8Ptr;
static ZKTextView* mTextViewW7Ptr;
static ZKTextView* mTextViewW6Ptr;
static ZKTextView* mTextViewW4Ptr;
static ZKTextView* mTextViewW3Ptr;
static ZKTextView* mTextViewW2Ptr;
static ZKTextView* mTextViewW1Ptr;
static ZKTextView* mTextViewBPtr;
static ZKTextView* mTextViewXPtr;
static ZKTextView* mTextViewCPtr;
static ZKButton* mButton12Ptr;
static ZKWindow* mjiluPtr;
static ZKTextView* mTextView68Ptr;
static ZKTextView* mTextView84Ptr;
static ZKTextView* mTextView83Ptr;
static ZKTextView* mTextView67Ptr;
static ZKTextView* mTextView82Ptr;
static ZKTextView* mTextView81Ptr;
static ZKTextView* mTextView80Ptr;
static ZKWindow* msetiaoPtr;
static ZKCircleBar* mCircleBar1Ptr;
static ZKTextView* mTextView66Ptr;
static ZKTextView* mTextView63Ptr;
static ZKTextView* mTextView62Ptr;
static ZKTextView* mTextView65Ptr;
static ZKTextView* mTextView64Ptr;
static ZKSeekBar* mSeekBar2Ptr;
static ZKTextView* mTextView61Ptr;
static ZKWindow* mliangduPtr;
static ZKWindow* mshengyinPtr;
static ZKTextView* mTextView60Ptr;
static ZKTextView* mTextView59Ptr;
static ZKTextView* mTextView58Ptr;
static ZKTextView* mTextView57Ptr;
static ZKSeekBar* mSeekBar1Ptr;
static ZKTextView* mTextView56Ptr;
static ZKTextView* mTextViewbatteryPtr;
static ZKTextView* mTextView50Ptr;
static ZKTextView* mTextView49Ptr;
static ZKTextView* mTextView48Ptr;
static ZKTextView* mTextView47Ptr;
static ZKTextView* mTextView46Ptr;
static ZKTextView* mTextView45Ptr;
static ZKTextView* mTextView44Ptr;
static ZKTextView* mTextView43Ptr;
static ZKTextView* mTextView42Ptr;
static ZKTextView* mTextView41Ptr;
static ZKTextView* mTextView40Ptr;
static ZKWindow* mBattery1Ptr;
static ZKTextView* mTextViewgif3Ptr;
static ZKTextView* mTextView39Ptr;
static ZKButton* mButton10Ptr;
static ZKTextView* mTextView38Ptr;
static ZKWindow* mtianjiashibaiPtr;
static ZKButton* mButton9Ptr;
static ZKTextView* mTextView37Ptr;
static ZKWindow* mtianjiachenggongPtr;
static ZKWindow* mtianjiazhongPtr;
static ZKWindow* mtianjiashebeiPtr;
static ZKButton* mButton8Ptr;
static ZKTextView* mTextView36Ptr;
static ZKTextView* mTextView35Ptr;
static ZKTextView* mTextViewgif2Ptr;
static ZKWindow* mCwifiPtr;
static ZKTextView* mNOBluetoothPtr;
static ZKWindow* mCBluetoothPtr;
static ZKTextView* mNOwifiPtr;
static ZKWindow* mWindowshibaiPtr;
static ZKWindow* mWindowchenggongPtr;
static ZKWindow* mWindowlianPtr;
static ZKButton* mButton7Ptr;
static ZKWindow* mWindowaugmentPtr;
static ZKTextView* mTextView34Ptr;
static ZKTextView* mTextViewQRcodePtr;
static ZKTextView* mTextViewfailPtr;
static ZKTextView* mTextViewsuccessPtr;
static ZKTextView* mTextViewCUOPtr;
static ZKTextView* mTextViewDUIPtr;
static ZKTextView* mTextViewgif1Ptr;
static ZKTextView* mTextViewlianjiePtr;
static ZKWindow* mWindowlianjiePtr;
static ZKTextView* mTextViewQRPtr;
static ZKWindow* mWindowQRCodePtr;
static ZKTextView* mTEXTVIEW_CONTENTPtr;
static ZKButton* mDeletePtr;
static ZKButton* mConfirmPtr;
static ZKButton* mNUM9Ptr;
static ZKButton* mNUM8Ptr;
static ZKButton* mNUM7Ptr;
static ZKButton* mNUM6Ptr;
static ZKButton* mNUM5Ptr;
static ZKButton* mNUM4Ptr;
static ZKButton* mNUM3Ptr;
static ZKButton* mNUM2Ptr;
static ZKButton* mNUM1Ptr;
static ZKButton* mNUM0Ptr;
static ZKButton* mButton21Ptr;
static ZKTextView* mTextViewgifPtr;
static ZKWindow* mWindowGIFPtr;
static ZKWindow* mWindowPasswordPtr;
static ZKTextView* mTextView32Ptr;
static ZKTextView* mTextView31Ptr;
static ZKTextView* mTextView30Ptr;
static ZKTextView* mTextView29Ptr;
static ZKButton* mButton5Ptr;
static ZKButton* mButton4Ptr;
static ZKTextView* mTextView28Ptr;
static ZKWindow* mWindowDrawerPtr;
static ZKWindow* mWindowHomePtr;
static ZKWindow* mWeatherPtr;
static ZKTextView* mTextWeekPtr;
static ZKTextView* mTextDatePtr;
static ZKTextView* mTemperaturePtr;
static ZKTextView* mTextTimePtr;
static ZKTextView* mTextView23Ptr;
static ZKTextView* mTextView22Ptr;
static ZKTextView* mTextView21Ptr;
static ZKTextView* mTextView20Ptr;
static ZKTextView* mTextView19Ptr;
static ZKTextView* mTextView18Ptr;
static ZKTextView* mTextView17Ptr;
static ZKTextView* mTextView16Ptr;
static ZKTextView* mTextView15Ptr;
static ZKTextView* mTextView14Ptr;
static ZKTextView* mTextView13Ptr;
static ZKWindow* mBatteryPtr;
static ZKTextView* mBluetoothPtr;
static ZKTextView* mWIFIPtr;
static mainActivity* mActivityPtr;

/*register activity*/
REGISTER_ACTIVITY(mainActivity);

typedef struct {
	int id; // 定时器ID ， 不能重复
	int time; // 定时器  时间间隔  单位 毫秒
}S_ACTIVITY_TIMEER;

#include "logic/mainLogic.cc"

/***********/
typedef struct {
    int id;
    const char *pApp;
} SAppInfo;

/**
 *点击跳转window
 */
static SAppInfo sAppInfoTab[] = {
//  { ID_MAIN_TEXT, "TextViewActivity" },
};

/***************/
typedef bool (*ButtonCallback)(ZKButton *pButton);
/**
 * button onclick表
 */
typedef struct {
    int id;
    ButtonCallback callback;
}S_ButtonCallback;

/*TAG:ButtonCallbackTab按键映射表*/
static S_ButtonCallback sButtonCallbackTab[] = {
    ID_MAIN_ButtonNext, onButtonClick_ButtonNext,
    ID_MAIN_ButtonPrev, onButtonClick_ButtonPrev,
    ID_MAIN_ButtonChange3, onButtonClick_ButtonChange3,
    ID_MAIN_Buttonshezhi, onButtonClick_Buttonshezhi,
    ID_MAIN_Buttonsuobiao, onButtonClick_Buttonsuobiao,
    ID_MAIN_Button12, onButtonClick_Button12,
    ID_MAIN_Button10, onButtonClick_Button10,
    ID_MAIN_Button9, onButtonClick_Button9,
    ID_MAIN_Button8, onButtonClick_Button8,
    ID_MAIN_Button7, onButtonClick_Button7,
    ID_MAIN_Delete, onButtonClick_Delete,
    ID_MAIN_Confirm, onButtonClick_Confirm,
    ID_MAIN_NUM9, onButtonClick_NUM9,
    ID_MAIN_NUM8, onButtonClick_NUM8,
    ID_MAIN_NUM7, onButtonClick_NUM7,
    ID_MAIN_NUM6, onButtonClick_NUM6,
    ID_MAIN_NUM5, onButtonClick_NUM5,
    ID_MAIN_NUM4, onButtonClick_NUM4,
    ID_MAIN_NUM3, onButtonClick_NUM3,
    ID_MAIN_NUM2, onButtonClick_NUM2,
    ID_MAIN_NUM1, onButtonClick_NUM1,
    ID_MAIN_NUM0, onButtonClick_NUM0,
    ID_MAIN_Button21, onButtonClick_Button21,
    ID_MAIN_Button5, onButtonClick_Button5,
    ID_MAIN_Button4, onButtonClick_Button4,
};
/***************/


typedef void (*SeekBarCallback)(ZKSeekBar *pSeekBar, int progress);
typedef struct {
    int id;
    SeekBarCallback callback;
}S_ZKSeekBarCallback;
/*TAG:SeekBarCallbackTab*/
static S_ZKSeekBarCallback SZKSeekBarCallbackTab[] = {
    ID_MAIN_SeekBar2, onProgressChanged_SeekBar2,
    ID_MAIN_SeekBar1, onProgressChanged_SeekBar1,
};


typedef int (*ListViewGetItemCountCallback)(const ZKListView *pListView);
typedef void (*ListViewobtainListItemDataCallback)(ZKListView *pListView,ZKListView::ZKListItem *pListItem, int index);
typedef void (*ListViewonItemClickCallback)(ZKListView *pListView, int index, int id);
typedef struct {
    int id;
    ListViewGetItemCountCallback getListItemCountCallback;
    ListViewobtainListItemDataCallback obtainListItemDataCallback;
    ListViewonItemClickCallback onItemClickCallback;
}S_ListViewFunctionsCallback;
/*TAG:ListViewFunctionsCallback*/
static S_ListViewFunctionsCallback SListViewFunctionsCallbackTab[] = {
};


typedef void (*SlideWindowItemClickCallback)(ZKSlideWindow *pSlideWindow, int index);
typedef struct {
    int id;
    SlideWindowItemClickCallback onSlideItemClickCallback;
}S_SlideWindowItemClickCallback;
/*TAG:SlideWindowFunctionsCallbackTab*/
static S_SlideWindowItemClickCallback SSlideWindowItemClickCallbackTab[] = {
};


typedef void (*EditTextInputCallback)(const std::string &text);
typedef struct {
    int id;
    EditTextInputCallback onEditTextChangedCallback;
}S_EditTextInputCallback;
/*TAG:EditTextInputCallback*/
static S_EditTextInputCallback SEditTextInputCallbackTab[] = {
};

typedef void (*VideoViewCallback)(ZKVideoView *pVideoView, int msg);
typedef struct {
    int id; //VideoView ID
    bool loop; // 是否是轮播类型
    int defaultvolume;//轮播类型时,默认视频音量
    VideoViewCallback onVideoViewCallback;
}S_VideoViewCallback;
/*TAG:VideoViewCallback*/
static S_VideoViewCallback SVideoViewCallbackTab[] = {
};


mainActivity::mainActivity() {
	//todo add init code here
	mVideoLoopIndex = -1;
	mVideoLoopErrorCount = 0;
}

mainActivity::~mainActivity() {
  //todo add init file here
  // 退出应用时需要反注册
    EASYUICONTEXT->unregisterGlobalTouchListener(this);
    unregisterProtocolDataUpdateListener(onProtocolDataUpdate);
    onUI_quit();
    mActivityPtr = NULL;
    mButtonNextPtr = NULL;
    mButtonPrevPtr = NULL;
    mTextViewDevNamePtr = NULL;
    mButtonChange3Ptr = NULL;
    mTextViewChange2Ptr = NULL;
    mTextViewChange1Ptr = NULL;
    mTextViewEnter1Ptr = NULL;
    mButtonshezhiPtr = NULL;
    mButtonsuobiaoPtr = NULL;
    mTextViewWFPtr = NULL;
    mTextViewWEPtr = NULL;
    mTextViewWBPtr = NULL;
    mTextViewWAPtr = NULL;
    mTextViewW9Ptr = NULL;
    mTextViewW8Ptr = NULL;
    mTextViewW7Ptr = NULL;
    mTextViewW6Ptr = NULL;
    mTextViewW4Ptr = NULL;
    mTextViewW3Ptr = NULL;
    mTextViewW2Ptr = NULL;
    mTextViewW1Ptr = NULL;
    mTextViewBPtr = NULL;
    mTextViewXPtr = NULL;
    mTextViewCPtr = NULL;
    mButton12Ptr = NULL;
    mjiluPtr = NULL;
    mTextView68Ptr = NULL;
    mTextView84Ptr = NULL;
    mTextView83Ptr = NULL;
    mTextView67Ptr = NULL;
    mTextView82Ptr = NULL;
    mTextView81Ptr = NULL;
    mTextView80Ptr = NULL;
    msetiaoPtr = NULL;
    mCircleBar1Ptr = NULL;
    mTextView66Ptr = NULL;
    mTextView63Ptr = NULL;
    mTextView62Ptr = NULL;
    mTextView65Ptr = NULL;
    mTextView64Ptr = NULL;
    mSeekBar2Ptr = NULL;
    mTextView61Ptr = NULL;
    mliangduPtr = NULL;
    mshengyinPtr = NULL;
    mTextView60Ptr = NULL;
    mTextView59Ptr = NULL;
    mTextView58Ptr = NULL;
    mTextView57Ptr = NULL;
    mSeekBar1Ptr = NULL;
    mTextView56Ptr = NULL;
    mTextViewbatteryPtr = NULL;
    mTextView50Ptr = NULL;
    mTextView49Ptr = NULL;
    mTextView48Ptr = NULL;
    mTextView47Ptr = NULL;
    mTextView46Ptr = NULL;
    mTextView45Ptr = NULL;
    mTextView44Ptr = NULL;
    mTextView43Ptr = NULL;
    mTextView42Ptr = NULL;
    mTextView41Ptr = NULL;
    mTextView40Ptr = NULL;
    mBattery1Ptr = NULL;
    mTextViewgif3Ptr = NULL;
    mTextView39Ptr = NULL;
    mButton10Ptr = NULL;
    mTextView38Ptr = NULL;
    mtianjiashibaiPtr = NULL;
    mButton9Ptr = NULL;
    mTextView37Ptr = NULL;
    mtianjiachenggongPtr = NULL;
    mtianjiazhongPtr = NULL;
    mtianjiashebeiPtr = NULL;
    mButton8Ptr = NULL;
    mTextView36Ptr = NULL;
    mTextView35Ptr = NULL;
    mTextViewgif2Ptr = NULL;
    mCwifiPtr = NULL;
    mNOBluetoothPtr = NULL;
    mCBluetoothPtr = NULL;
    mNOwifiPtr = NULL;
    mWindowshibaiPtr = NULL;
    mWindowchenggongPtr = NULL;
    mWindowlianPtr = NULL;
    mButton7Ptr = NULL;
    mWindowaugmentPtr = NULL;
    mTextView34Ptr = NULL;
    mTextViewQRcodePtr = NULL;
    mTextViewfailPtr = NULL;
    mTextViewsuccessPtr = NULL;
    mTextViewCUOPtr = NULL;
    mTextViewDUIPtr = NULL;
    mTextViewgif1Ptr = NULL;
    mTextViewlianjiePtr = NULL;
    mWindowlianjiePtr = NULL;
    mTextViewQRPtr = NULL;
    mWindowQRCodePtr = NULL;
    mTEXTVIEW_CONTENTPtr = NULL;
    mDeletePtr = NULL;
    mConfirmPtr = NULL;
    mNUM9Ptr = NULL;
    mNUM8Ptr = NULL;
    mNUM7Ptr = NULL;
    mNUM6Ptr = NULL;
    mNUM5Ptr = NULL;
    mNUM4Ptr = NULL;
    mNUM3Ptr = NULL;
    mNUM2Ptr = NULL;
    mNUM1Ptr = NULL;
    mNUM0Ptr = NULL;
    mButton21Ptr = NULL;
    mTextViewgifPtr = NULL;
    mWindowGIFPtr = NULL;
    mWindowPasswordPtr = NULL;
    mTextView32Ptr = NULL;
    mTextView31Ptr = NULL;
    mTextView30Ptr = NULL;
    mTextView29Ptr = NULL;
    mButton5Ptr = NULL;
    mButton4Ptr = NULL;
    mTextView28Ptr = NULL;
    mWindowDrawerPtr = NULL;
    mWindowHomePtr = NULL;
    mWeatherPtr = NULL;
    mTextWeekPtr = NULL;
    mTextDatePtr = NULL;
    mTemperaturePtr = NULL;
    mTextTimePtr = NULL;
    mTextView23Ptr = NULL;
    mTextView22Ptr = NULL;
    mTextView21Ptr = NULL;
    mTextView20Ptr = NULL;
    mTextView19Ptr = NULL;
    mTextView18Ptr = NULL;
    mTextView17Ptr = NULL;
    mTextView16Ptr = NULL;
    mTextView15Ptr = NULL;
    mTextView14Ptr = NULL;
    mTextView13Ptr = NULL;
    mBatteryPtr = NULL;
    mBluetoothPtr = NULL;
    mWIFIPtr = NULL;
}

const char* mainActivity::getAppName() const{
	return "main.ftu";
}

//TAG:onCreate
void mainActivity::onCreate() {
	Activity::onCreate();
    mButtonNextPtr = (ZKButton*)findControlByID(ID_MAIN_ButtonNext);
    mButtonPrevPtr = (ZKButton*)findControlByID(ID_MAIN_ButtonPrev);
    mTextViewDevNamePtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewDevName);
    mButtonChange3Ptr = (ZKButton*)findControlByID(ID_MAIN_ButtonChange3);
    mTextViewChange2Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewChange2);
    mTextViewChange1Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewChange1);
    mTextViewEnter1Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewEnter1);
    mButtonshezhiPtr = (ZKButton*)findControlByID(ID_MAIN_Buttonshezhi);
    mButtonsuobiaoPtr = (ZKButton*)findControlByID(ID_MAIN_Buttonsuobiao);
    mTextViewWFPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewWF);
    mTextViewWEPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewWE);
    mTextViewWBPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewWB);
    mTextViewWAPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewWA);
    mTextViewW9Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW9);
    mTextViewW8Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW8);
    mTextViewW7Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW7);
    mTextViewW6Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW6);
    mTextViewW4Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW4);
    mTextViewW3Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW3);
    mTextViewW2Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW2);
    mTextViewW1Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewW1);
    mTextViewBPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewB);
    mTextViewXPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewX);
    mTextViewCPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewC);
    mButton12Ptr = (ZKButton*)findControlByID(ID_MAIN_Button12);
    mjiluPtr = (ZKWindow*)findControlByID(ID_MAIN_jilu);
    mTextView68Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView68);
    mTextView84Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView84);
    mTextView83Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView83);
    mTextView67Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView67);
    mTextView82Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView82);
    mTextView81Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView81);
    mTextView80Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView80);
    msetiaoPtr = (ZKWindow*)findControlByID(ID_MAIN_setiao);
    mCircleBar1Ptr = (ZKCircleBar*)findControlByID(ID_MAIN_CircleBar1);
    mTextView66Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView66);
    mTextView63Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView63);
    mTextView62Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView62);
    mTextView65Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView65);
    mTextView64Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView64);
    mSeekBar2Ptr = (ZKSeekBar*)findControlByID(ID_MAIN_SeekBar2);if(mSeekBar2Ptr!= NULL){mSeekBar2Ptr->setSeekBarChangeListener(this);}
    mTextView61Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView61);
    mliangduPtr = (ZKWindow*)findControlByID(ID_MAIN_liangdu);
    mshengyinPtr = (ZKWindow*)findControlByID(ID_MAIN_shengyin);
    mTextView60Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView60);
    mTextView59Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView59);
    mTextView58Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView58);
    mTextView57Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView57);
    mSeekBar1Ptr = (ZKSeekBar*)findControlByID(ID_MAIN_SeekBar1);if(mSeekBar1Ptr!= NULL){mSeekBar1Ptr->setSeekBarChangeListener(this);}
    mTextView56Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView56);
    mTextViewbatteryPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewbattery);
    mTextView50Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView50);
    mTextView49Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView49);
    mTextView48Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView48);
    mTextView47Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView47);
    mTextView46Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView46);
    mTextView45Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView45);
    mTextView44Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView44);
    mTextView43Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView43);
    mTextView42Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView42);
    mTextView41Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView41);
    mTextView40Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView40);
    mBattery1Ptr = (ZKWindow*)findControlByID(ID_MAIN_Battery1);
    mTextViewgif3Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewgif3);
    mTextView39Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView39);
    mButton10Ptr = (ZKButton*)findControlByID(ID_MAIN_Button10);
    mTextView38Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView38);
    mtianjiashibaiPtr = (ZKWindow*)findControlByID(ID_MAIN_tianjiashibai);
    mButton9Ptr = (ZKButton*)findControlByID(ID_MAIN_Button9);
    mTextView37Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView37);
    mtianjiachenggongPtr = (ZKWindow*)findControlByID(ID_MAIN_tianjiachenggong);
    mtianjiazhongPtr = (ZKWindow*)findControlByID(ID_MAIN_tianjiazhong);
    mtianjiashebeiPtr = (ZKWindow*)findControlByID(ID_MAIN_tianjiashebei);
    mButton8Ptr = (ZKButton*)findControlByID(ID_MAIN_Button8);
    mTextView36Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView36);
    mTextView35Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView35);
    mTextViewgif2Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewgif2);
    mCwifiPtr = (ZKWindow*)findControlByID(ID_MAIN_Cwifi);
    mNOBluetoothPtr = (ZKTextView*)findControlByID(ID_MAIN_NOBluetooth);
    mCBluetoothPtr = (ZKWindow*)findControlByID(ID_MAIN_CBluetooth);
    mNOwifiPtr = (ZKTextView*)findControlByID(ID_MAIN_NOwifi);
    mWindowshibaiPtr = (ZKWindow*)findControlByID(ID_MAIN_Windowshibai);
    mWindowchenggongPtr = (ZKWindow*)findControlByID(ID_MAIN_Windowchenggong);
    mWindowlianPtr = (ZKWindow*)findControlByID(ID_MAIN_Windowlian);
    mButton7Ptr = (ZKButton*)findControlByID(ID_MAIN_Button7);
    mWindowaugmentPtr = (ZKWindow*)findControlByID(ID_MAIN_Windowaugment);
    mTextView34Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView34);
    mTextViewQRcodePtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewQRcode);
    mTextViewfailPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewfail);
    mTextViewsuccessPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewsuccess);
    mTextViewCUOPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewCUO);
    mTextViewDUIPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewDUI);
    mTextViewgif1Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextViewgif1);
    mTextViewlianjiePtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewlianjie);
    mWindowlianjiePtr = (ZKWindow*)findControlByID(ID_MAIN_Windowlianjie);
    mTextViewQRPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewQR);
    mWindowQRCodePtr = (ZKWindow*)findControlByID(ID_MAIN_WindowQRCode);
    mTEXTVIEW_CONTENTPtr = (ZKTextView*)findControlByID(ID_MAIN_TEXTVIEW_CONTENT);
    mDeletePtr = (ZKButton*)findControlByID(ID_MAIN_Delete);
    mConfirmPtr = (ZKButton*)findControlByID(ID_MAIN_Confirm);
    mNUM9Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM9);
    mNUM8Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM8);
    mNUM7Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM7);
    mNUM6Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM6);
    mNUM5Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM5);
    mNUM4Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM4);
    mNUM3Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM3);
    mNUM2Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM2);
    mNUM1Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM1);
    mNUM0Ptr = (ZKButton*)findControlByID(ID_MAIN_NUM0);
    mButton21Ptr = (ZKButton*)findControlByID(ID_MAIN_Button21);
    mTextViewgifPtr = (ZKTextView*)findControlByID(ID_MAIN_TextViewgif);
    mWindowGIFPtr = (ZKWindow*)findControlByID(ID_MAIN_WindowGIF);
    mWindowPasswordPtr = (ZKWindow*)findControlByID(ID_MAIN_WindowPassword);
    mTextView32Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView32);
    mTextView31Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView31);
    mTextView30Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView30);
    mTextView29Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView29);
    mButton5Ptr = (ZKButton*)findControlByID(ID_MAIN_Button5);
    mButton4Ptr = (ZKButton*)findControlByID(ID_MAIN_Button4);
    mTextView28Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView28);
    mWindowDrawerPtr = (ZKWindow*)findControlByID(ID_MAIN_WindowDrawer);
    mWindowHomePtr = (ZKWindow*)findControlByID(ID_MAIN_WindowHome);
    mWeatherPtr = (ZKWindow*)findControlByID(ID_MAIN_Weather);
    mTextWeekPtr = (ZKTextView*)findControlByID(ID_MAIN_TextWeek);
    mTextDatePtr = (ZKTextView*)findControlByID(ID_MAIN_TextDate);
    mTemperaturePtr = (ZKTextView*)findControlByID(ID_MAIN_Temperature);
    mTextTimePtr = (ZKTextView*)findControlByID(ID_MAIN_TextTime);
    mTextView23Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView23);
    mTextView22Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView22);
    mTextView21Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView21);
    mTextView20Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView20);
    mTextView19Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView19);
    mTextView18Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView18);
    mTextView17Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView17);
    mTextView16Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView16);
    mTextView15Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView15);
    mTextView14Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView14);
    mTextView13Ptr = (ZKTextView*)findControlByID(ID_MAIN_TextView13);
    mBatteryPtr = (ZKWindow*)findControlByID(ID_MAIN_Battery);
    mBluetoothPtr = (ZKTextView*)findControlByID(ID_MAIN_Bluetooth);
    mWIFIPtr = (ZKTextView*)findControlByID(ID_MAIN_WIFI);
	mActivityPtr = this;
	onUI_init();
  registerProtocolDataUpdateListener(onProtocolDataUpdate);
  rigesterActivityTimer();
}

void mainActivity::onClick(ZKBase *pBase) {
	//TODO: add widget onClik code 
    int buttonTablen = sizeof(sButtonCallbackTab) / sizeof(S_ButtonCallback);
    for (int i = 0; i < buttonTablen; ++i) {
        if (sButtonCallbackTab[i].id == pBase->getID()) {
            if (sButtonCallbackTab[i].callback((ZKButton*)pBase)) {
            	return;
            }
            break;
        }
    }


    int len = sizeof(sAppInfoTab) / sizeof(sAppInfoTab[0]);
    for (int i = 0; i < len; ++i) {
        if (sAppInfoTab[i].id == pBase->getID()) {
            EASYUICONTEXT->openActivity(sAppInfoTab[i].pApp);
            return;
        }
    }

	Activity::onClick(pBase);
}

void mainActivity::onResume() {
	Activity::onResume();
	EASYUICONTEXT->registerGlobalTouchListener(this);
	startVideoLoopPlayback();
	onUI_show();
}

void mainActivity::onPause() {
	Activity::onPause();
	EASYUICONTEXT->unregisterGlobalTouchListener(this);
	stopVideoLoopPlayback();
	onUI_hide();
}

void mainActivity::onIntent(const Intent *intentPtr) {
	Activity::onIntent(intentPtr);
	onUI_intent(intentPtr);
}

bool mainActivity::onTimer(int id) {
	return onUI_Timer(id);
}

void mainActivity::onProgressChanged(ZKSeekBar *pSeekBar, int progress){

    int seekBarTablen = sizeof(SZKSeekBarCallbackTab) / sizeof(S_ZKSeekBarCallback);
    for (int i = 0; i < seekBarTablen; ++i) {
        if (SZKSeekBarCallbackTab[i].id == pSeekBar->getID()) {
            SZKSeekBarCallbackTab[i].callback(pSeekBar, progress);
            break;
        }
    }
}

int mainActivity::getListItemCount(const ZKListView *pListView) const{
    int tablen = sizeof(SListViewFunctionsCallbackTab) / sizeof(S_ListViewFunctionsCallback);
    for (int i = 0; i < tablen; ++i) {
        if (SListViewFunctionsCallbackTab[i].id == pListView->getID()) {
            return SListViewFunctionsCallbackTab[i].getListItemCountCallback(pListView);
            break;
        }
    }
    return 0;
}

void mainActivity::obtainListItemData(ZKListView *pListView,ZKListView::ZKListItem *pListItem, int index){
    int tablen = sizeof(SListViewFunctionsCallbackTab) / sizeof(S_ListViewFunctionsCallback);
    for (int i = 0; i < tablen; ++i) {
        if (SListViewFunctionsCallbackTab[i].id == pListView->getID()) {
            SListViewFunctionsCallbackTab[i].obtainListItemDataCallback(pListView, pListItem, index);
            break;
        }
    }
}

void mainActivity::onItemClick(ZKListView *pListView, int index, int id){
    int tablen = sizeof(SListViewFunctionsCallbackTab) / sizeof(S_ListViewFunctionsCallback);
    for (int i = 0; i < tablen; ++i) {
        if (SListViewFunctionsCallbackTab[i].id == pListView->getID()) {
            SListViewFunctionsCallbackTab[i].onItemClickCallback(pListView, index, id);
            break;
        }
    }
}

void mainActivity::onSlideItemClick(ZKSlideWindow *pSlideWindow, int index) {
    int tablen = sizeof(SSlideWindowItemClickCallbackTab) / sizeof(S_SlideWindowItemClickCallback);
    for (int i = 0; i < tablen; ++i) {
        if (SSlideWindowItemClickCallbackTab[i].id == pSlideWindow->getID()) {
            SSlideWindowItemClickCallbackTab[i].onSlideItemClickCallback(pSlideWindow, index);
            break;
        }
    }
}

bool mainActivity::onTouchEvent(const MotionEvent &ev) {
    return onmainActivityTouchEvent(ev);
}

void mainActivity::onTextChanged(ZKTextView *pTextView, const std::string &text) {
    int tablen = sizeof(SEditTextInputCallbackTab) / sizeof(S_EditTextInputCallback);
    for (int i = 0; i < tablen; ++i) {
        if (SEditTextInputCallbackTab[i].id == pTextView->getID()) {
            SEditTextInputCallbackTab[i].onEditTextChangedCallback(text);
            break;
        }
    }
}

void mainActivity::rigesterActivityTimer() {
    int tablen = sizeof(REGISTER_ACTIVITY_TIMER_TAB) / sizeof(S_ACTIVITY_TIMEER);
    for (int i = 0; i < tablen; ++i) {
        S_ACTIVITY_TIMEER temp = REGISTER_ACTIVITY_TIMER_TAB[i];
        registerTimer(temp.id, temp.time);
    }
}


void mainActivity::onVideoPlayerMessage(ZKVideoView *pVideoView, int msg) {
    int tablen = sizeof(SVideoViewCallbackTab) / sizeof(S_VideoViewCallback);
    for (int i = 0; i < tablen; ++i) {
        if (SVideoViewCallbackTab[i].id == pVideoView->getID()) {
        	if (SVideoViewCallbackTab[i].loop) {
                //循环播放
        		videoLoopPlayback(pVideoView, msg, i);
        	} else if (SVideoViewCallbackTab[i].onVideoViewCallback != NULL){
        	    SVideoViewCallbackTab[i].onVideoViewCallback(pVideoView, msg);
        	}
            break;
        }
    }
}

void mainActivity::videoLoopPlayback(ZKVideoView *pVideoView, int msg, size_t callbackTabIndex) {

	switch (msg) {
	case ZKVideoView::E_MSGTYPE_VIDEO_PLAY_STARTED:
		LOGD("ZKVideoView::E_MSGTYPE_VIDEO_PLAY_STARTED\n");
    if (callbackTabIndex >= (sizeof(SVideoViewCallbackTab)/sizeof(S_VideoViewCallback))) {
      break;
    }
		pVideoView->setVolume(SVideoViewCallbackTab[callbackTabIndex].defaultvolume / 10.0);
		mVideoLoopErrorCount = 0;
		break;
	case ZKVideoView::E_MSGTYPE_VIDEO_PLAY_ERROR:
		/**错误处理 */
		++mVideoLoopErrorCount;
		if (mVideoLoopErrorCount > 100) {
			LOGD("video loop error counts > 100, quit loop playback !");
            break;
		} //不用break, 继续尝试播放下一个
	case ZKVideoView::E_MSGTYPE_VIDEO_PLAY_COMPLETED:
		LOGD("ZKVideoView::E_MSGTYPE_VIDEO_PLAY_COMPLETED\n");
        std::vector<std::string> videolist;
        std::string fileName(getAppName());
        if (fileName.size() < 4) {
             LOGD("getAppName size < 4, ignore!");
             break;
        }
        fileName = fileName.substr(0, fileName.length() - 4) + "_video_list.txt";
        fileName = "/mnt/extsd/" + fileName;
        if (!parseVideoFileList(fileName.c_str(), videolist)) {
            LOGD("parseVideoFileList failed !");
		    break;
        }
		if (pVideoView && !videolist.empty()) {
			mVideoLoopIndex = (mVideoLoopIndex + 1) % videolist.size();
			pVideoView->play(videolist[mVideoLoopIndex].c_str());
		}
		break;
	}
}

void mainActivity::startVideoLoopPlayback() {
    int tablen = sizeof(SVideoViewCallbackTab) / sizeof(S_VideoViewCallback);
    for (int i = 0; i < tablen; ++i) {
    	if (SVideoViewCallbackTab[i].loop) {
    		ZKVideoView* videoView = (ZKVideoView*)findControlByID(SVideoViewCallbackTab[i].id);
    		if (!videoView) {
    			return;
    		}
    		//循环播放
    		videoLoopPlayback(videoView, ZKVideoView::E_MSGTYPE_VIDEO_PLAY_COMPLETED, i);
    		return;
    	}
    }
}

void mainActivity::stopVideoLoopPlayback() {
    int tablen = sizeof(SVideoViewCallbackTab) / sizeof(S_VideoViewCallback);
    for (int i = 0; i < tablen; ++i) {
    	if (SVideoViewCallbackTab[i].loop) {
    		ZKVideoView* videoView = (ZKVideoView*)findControlByID(SVideoViewCallbackTab[i].id);
    		if (!videoView) {
    			return;
    		}
    		if (videoView->isPlaying()) {
    		    videoView->stop();
    		}
    		return;
    	}
    }
}

bool mainActivity::parseVideoFileList(const char *pFileListPath, std::vector<string>& mediaFileList) {
	mediaFileList.clear();
	if (NULL == pFileListPath || 0 == strlen(pFileListPath)) {
        LOGD("video file list is null!");
		return false;
	}

	ifstream is(pFileListPath, ios_base::in);
	if (!is.is_open()) {
		LOGD("cann't open file %s \n", pFileListPath);
		return false;
	}
	char tmp[1024] = {0};
	while (is.getline(tmp, sizeof(tmp))) {
		string str = tmp;
		removeCharFromString(str, '\"');
		removeCharFromString(str, '\r');
		removeCharFromString(str, '\n');
		if (str.size() > 1) {
     		mediaFileList.push_back(str.c_str());
		}
	}
  LOGD("(f:%s, l:%d) parse fileList[%s], get [%d]files", __FUNCTION__,
      __LINE__, pFileListPath, int(mediaFileList.size()));
  for (std::vector<string>::size_type i = 0; i < mediaFileList.size(); i++) {
    LOGD("file[%u]:[%s]", ::size_t(i), mediaFileList[i].c_str());
  }
	is.close();

	return true;
}

int mainActivity::removeCharFromString(string& nString, char c) {
    string::size_type   pos;
    while(1) {
        pos = nString.find(c);
        if(pos != string::npos) {
            nString.erase(pos, 1);
        } else {
            break;
        }
    }
    return (int)nString.size();
}

void mainActivity::registerUserTimer(int id, int time) {
	registerTimer(id, time);
}

void mainActivity::unregisterUserTimer(int id) {
	unregisterTimer(id);
}

void mainActivity::resetUserTimer(int id, int time) {
	resetTimer(id, time);
}

ZKBase* mainActivity::findControlByID(int id) {
	return BaseApp::findControlByID(id);
}