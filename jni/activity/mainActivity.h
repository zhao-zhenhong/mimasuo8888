/***********************************************
/gen auto by zuitools
***********************************************/
#ifndef __MAINACTIVITY_H__
#define __MAINACTIVITY_H__


#include "app/Activity.h"
#include "entry/EasyUIContext.h"

#include "uart/ProtocolData.h"
#include "uart/ProtocolParser.h"

#include "utils/Log.h"
#include "control/ZKDigitalClock.h"
#include "control/ZKButton.h"
#include "control/ZKCircleBar.h"
#include "control/ZKDiagram.h"
#include "control/ZKListView.h"
#include "control/ZKPointer.h"
#include "control/ZKQRCode.h"
#include "control/ZKTextView.h"
#include "control/ZKSeekBar.h"
#include "control/ZKEditText.h"
#include "control/ZKVideoView.h"
#include "window/ZKSlideWindow.h"

/*TAG:Macro宏ID*/
#define ID_MAIN_ButtonNext    20022
#define ID_MAIN_ButtonPrev    20021
#define ID_MAIN_TextViewDevName    50093
#define ID_MAIN_ButtonChange3    20025
#define ID_MAIN_TextViewChange2    50090
#define ID_MAIN_TextViewChange1    50089
#define ID_MAIN_TextViewEnter1    50048
#define ID_MAIN_Buttonshezhi    20002
#define ID_MAIN_Buttonsuobiao    20001
#define ID_MAIN_TextViewWF    50001
#define ID_MAIN_TextViewWE    50008
#define ID_MAIN_TextViewWB    50010
#define ID_MAIN_TextViewWA    50002
#define ID_MAIN_TextViewW9    50012
#define ID_MAIN_TextViewW8    50003
#define ID_MAIN_TextViewW7    50009
#define ID_MAIN_TextViewW6    50005
#define ID_MAIN_TextViewW4    50007
#define ID_MAIN_TextViewW3    50004
#define ID_MAIN_TextViewW2    50006
#define ID_MAIN_TextViewW1    50011
#define ID_MAIN_TextViewB    50095
#define ID_MAIN_TextViewX    50096
#define ID_MAIN_TextViewC    50094
#define ID_MAIN_Button12    20026
#define ID_MAIN_jilu    110025
#define ID_MAIN_TextView68    50088
#define ID_MAIN_TextView84    50104
#define ID_MAIN_TextView83    50103
#define ID_MAIN_TextView67    50087
#define ID_MAIN_TextView82    50102
#define ID_MAIN_TextView81    50101
#define ID_MAIN_TextView80    50100
#define ID_MAIN_setiao    110024
#define ID_MAIN_CircleBar1    130001
#define ID_MAIN_TextView66    50086
#define ID_MAIN_TextView63    50083
#define ID_MAIN_TextView62    50082
#define ID_MAIN_TextView65    50085
#define ID_MAIN_TextView64    50084
#define ID_MAIN_SeekBar2    91002
#define ID_MAIN_TextView61    50081
#define ID_MAIN_liangdu    110023
#define ID_MAIN_shengyin    110022
#define ID_MAIN_TextView60    50080
#define ID_MAIN_TextView59    50079
#define ID_MAIN_TextView58    50078
#define ID_MAIN_TextView57    50077
#define ID_MAIN_SeekBar1    91001
#define ID_MAIN_TextView56    50076
#define ID_MAIN_TextViewbattery    50070
#define ID_MAIN_TextView50    50069
#define ID_MAIN_TextView49    50068
#define ID_MAIN_TextView48    50067
#define ID_MAIN_TextView47    50066
#define ID_MAIN_TextView46    50065
#define ID_MAIN_TextView45    50064
#define ID_MAIN_TextView44    50063
#define ID_MAIN_TextView43    50062
#define ID_MAIN_TextView42    50061
#define ID_MAIN_TextView41    50060
#define ID_MAIN_TextView40    50059
#define ID_MAIN_Battery1    110021
#define ID_MAIN_TextViewgif3    50058
#define ID_MAIN_TextView39    50057
#define ID_MAIN_Button10    20024
#define ID_MAIN_TextView38    50056
#define ID_MAIN_tianjiashibai    110019
#define ID_MAIN_Button9    20020
#define ID_MAIN_TextView37    50055
#define ID_MAIN_tianjiachenggong    110018
#define ID_MAIN_tianjiazhong    110017
#define ID_MAIN_tianjiashebei    110011
#define ID_MAIN_Button8    20014
#define ID_MAIN_TextView36    50054
#define ID_MAIN_TextView35    50053
#define ID_MAIN_TextViewgif2    50052
#define ID_MAIN_Cwifi    110015
#define ID_MAIN_NOBluetooth    50051
#define ID_MAIN_CBluetooth    110016
#define ID_MAIN_NOwifi    50050
#define ID_MAIN_Windowshibai    110014
#define ID_MAIN_Windowchenggong    110013
#define ID_MAIN_Windowlian    110012
#define ID_MAIN_Button7    20006
#define ID_MAIN_Windowaugment    110010
#define ID_MAIN_TextView34    50049
#define ID_MAIN_TextViewQRcode    50047
#define ID_MAIN_TextViewfail    50046
#define ID_MAIN_TextViewsuccess    50045
#define ID_MAIN_TextViewCUO    50044
#define ID_MAIN_TextViewDUI    50043
#define ID_MAIN_TextViewgif1    50040
#define ID_MAIN_TextViewlianjie    50042
#define ID_MAIN_Windowlianjie    110009
#define ID_MAIN_TextViewQR    50039
#define ID_MAIN_WindowQRCode    110008
#define ID_MAIN_TEXTVIEW_CONTENT    50000
#define ID_MAIN_Delete    20009
#define ID_MAIN_Confirm    20008
#define ID_MAIN_NUM9    20019
#define ID_MAIN_NUM8    20018
#define ID_MAIN_NUM7    20017
#define ID_MAIN_NUM6    20016
#define ID_MAIN_NUM5    20015
#define ID_MAIN_NUM4    20013
#define ID_MAIN_NUM3    20012
#define ID_MAIN_NUM2    20011
#define ID_MAIN_NUM1    20010
#define ID_MAIN_NUM0    20007
#define ID_MAIN_Button21    20023
#define ID_MAIN_TextViewgif    50041
#define ID_MAIN_WindowGIF    110005
#define ID_MAIN_WindowPassword    110006
#define ID_MAIN_TextView32    50038
#define ID_MAIN_TextView31    50037
#define ID_MAIN_TextView30    50036
#define ID_MAIN_TextView29    50035
#define ID_MAIN_Button5    20005
#define ID_MAIN_Button4    20004
#define ID_MAIN_TextView28    50034
#define ID_MAIN_WindowDrawer    110007
#define ID_MAIN_WindowHome    110001
#define ID_MAIN_Weather    110002
#define ID_MAIN_TextWeek    50029
#define ID_MAIN_TextDate    50028
#define ID_MAIN_Temperature    50027
#define ID_MAIN_TextTime    50026
#define ID_MAIN_TextView23    50025
#define ID_MAIN_TextView22    50024
#define ID_MAIN_TextView21    50023
#define ID_MAIN_TextView20    50022
#define ID_MAIN_TextView19    50021
#define ID_MAIN_TextView18    50020
#define ID_MAIN_TextView17    50019
#define ID_MAIN_TextView16    50018
#define ID_MAIN_TextView15    50017
#define ID_MAIN_TextView14    50016
#define ID_MAIN_TextView13    50015
#define ID_MAIN_Battery    110003
#define ID_MAIN_Bluetooth    50014
#define ID_MAIN_WIFI    50013
/*TAG:Macro宏ID END*/

class mainActivity : public Activity, 
                     public ZKSeekBar::ISeekBarChangeListener, 
                     public ZKListView::IItemClickListener,
                     public ZKListView::AbsListAdapter,
                     public ZKSlideWindow::ISlideItemClickListener,
                     public EasyUIContext::ITouchListener,
                     public ZKEditText::ITextChangeListener,
                     public ZKVideoView::IVideoPlayerMessageListener
{
public:
    mainActivity();
    virtual ~mainActivity();

    /**
     * 注册定时器
     */
	void registerUserTimer(int id, int time);
	/**
	 * 取消定时器
	 */
	void unregisterUserTimer(int id);
	/**
	 * 重置定时器
	 */
	void resetUserTimer(int id, int time);

	ZKBase* findControlByID(int id);

protected:
    /*TAG:PROTECTED_FUNCTION*/
    virtual const char* getAppName() const;
    virtual void onCreate();
    virtual void onClick(ZKBase *pBase);
    virtual void onResume();
    virtual void onPause();
    virtual void onIntent(const Intent *intentPtr);
    virtual bool onTimer(int id);

    virtual void onProgressChanged(ZKSeekBar *pSeekBar, int progress);

    virtual int getListItemCount(const ZKListView *pListView) const;
    virtual void obtainListItemData(ZKListView *pListView, ZKListView::ZKListItem *pListItem, int index);
    virtual void onItemClick(ZKListView *pListView, int index, int subItemIndex);

    virtual void onSlideItemClick(ZKSlideWindow *pSlideWindow, int index);

    virtual bool onTouchEvent(const MotionEvent &ev);

    virtual void onTextChanged(ZKTextView *pTextView, const string &text);

    void rigesterActivityTimer();

    virtual void onVideoPlayerMessage(ZKVideoView *pVideoView, int msg);
    void videoLoopPlayback(ZKVideoView *pVideoView, int msg, size_t callbackTabIndex);
    void startVideoLoopPlayback();
    void stopVideoLoopPlayback();
    bool parseVideoFileList(const char *pFileListPath, std::vector<string>& mediaFileList);
    int removeCharFromString(string& nString, char c);


private:
    /*TAG:PRIVATE_VARIABLE*/
    int mVideoLoopIndex;
    int mVideoLoopErrorCount;

};

#endif