/*
 * ProtocolData.h
 *
 *  Created on: Sep 7, 2017
 *      Author: guoxs
 */

#ifndef _UART_PROTOCOL_DATA_H_
#define _UART_PROTOCOL_DATA_H_

#include <string>
#include "CommDef.h"

/******************** CmdID ***********************/
#define CMDID_POWER							0x0
/**************************************************/

/******************** 错误码 Error code ***********************/
#define ERROR_CODE_CMDID			1
/**************************************************/
#define NAME_SIZE 100

extern bool Protocol_on_flag;//协议在通讯标志

typedef struct {
	BYTE netState;//网络状态
	UINT16 year;
	BYTE month;
	BYTE day;
	BYTE hour;
	BYTE minute;
	BYTE second;
	BYTE temperature;
	BYTE Humidity;
	BYTE Weather;
	BYTE DevNumT;
	BYTE BatteryCapacity;
	BYTE FactoryState;
	BYTE HardwareVertion[6];
	BYTE DevStyle[NAME_SIZE][4];
	BYTE DevN[NAME_SIZE][4];
	BYTE DevState;
} SProtocolData;

extern BYTE DevStyle[NAME_SIZE][4];
extern BYTE DevN[NAME_SIZE][4];
extern BYTE DevNumT;

#endif /* _UART_PROTOCOL_DATA_H_ */
