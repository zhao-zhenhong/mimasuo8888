/*
 * ProtocolSender.cpp
 *
 *  Created on: Sep 8, 2017
 *      Author: guoxs
 */
#include "uart/ProtocolData.h"
#include "uart/ProtocolSender.h"
#include "uart/UartContext.h"
#include "utils/Log.h"
#include <stdio.h>

extern BYTE getCheckSum(const BYTE *pData, int len);

/**
 * 需要根据协议格式进行拼接，以下只是个模板
 * Need to be spliced ​​according to the agreement format, the following is just a template
 */
bool sendProtocol(const UINT16 cmdID, const BYTE *pData, BYTE len) {
	Protocol_on_flag = true;

	if (len + DATA_PACKAGE_MIN_LEN > 256) {
		LOGE("sendProtocol data is too len !!!\n");
		return false;
	}

	BYTE dataBuf[256];

	dataBuf[0] = CMD_HEAD1;
	dataBuf[1] = CMD_HEAD2;			// 同步帧头 Sync frame header

	dataBuf[2] = len + CmdID_LEN + CheckSum_LEN;

	dataBuf[3] = cmdID;		// 命令字节 Command byte

	UINT frameLen = 4;

	// 数据 Data
	for (int i = 0; i < len; ++i) {
		dataBuf[frameLen] = pData[i];
		frameLen++;
	}

#ifdef PRO_SUPPORT_CHECK_SUM
	UINT16 checksum_temp;

	//checksum_temp = getCheckSum(dataBuf, frameLen);
	for (int i = 2; i < frameLen; ++i) {
		checksum_temp += dataBuf[i];
	}
#ifndef CHECK_SUM_ALL
	checksum_temp = ~checksum_temp + 1;
#endif
	//LOGD("11111111111: %x\n ", checksum_temp);
	// 校验码 Checksum
	dataBuf[frameLen] = HIBYTE(checksum_temp);
	frameLen++;
	dataBuf[frameLen] = LOBYTE(checksum_temp);
	frameLen++;
#endif

	Protocol_on_flag = false;
	return UARTCONTEXT->send(dataBuf, frameLen);
}
