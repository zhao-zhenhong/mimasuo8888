/*
 * ProtocolParser.cpp
 *
 *  Created on: Sep 7, 2017
 *      Author: guoxs
 */
#include <vector>
#include <string.h>
#include <system/Mutex.h>
#include "CommDef.h"
#include "uart/ProtocolParser.h"
#include "utils/Log.h"

static Mutex sLock;
static std::vector<OnProtocolDataUpdateFun> sProtocolDataUpdateListenerList;

void registerProtocolDataUpdateListener(OnProtocolDataUpdateFun pListener) {
	Mutex::Autolock _l(sLock);
	LOGD("registerProtocolDataUpdateListener\n");
	if (pListener != NULL) {
		sProtocolDataUpdateListenerList.push_back(pListener);
	}
}

void unregisterProtocolDataUpdateListener(OnProtocolDataUpdateFun pListener) {
	Mutex::Autolock _l(sLock);
	LOGD("unregisterProtocolDataUpdateListener\n");
	if (pListener != NULL) {
		std::vector<OnProtocolDataUpdateFun>::iterator iter = sProtocolDataUpdateListenerList.begin();
		for (; iter != sProtocolDataUpdateListenerList.end(); iter++) {
			if ((*iter) == pListener) {
				sProtocolDataUpdateListenerList.erase(iter);
				return;
			}
		}
	}
}

static void notifyProtocolDataUpdate(const SProtocolData &data) {
	Mutex::Autolock _l(sLock);
	std::vector<OnProtocolDataUpdateFun>::const_iterator iter = sProtocolDataUpdateListenerList.begin();
	for (; iter != sProtocolDataUpdateListenerList.end(); iter++) {
		(*iter)(data);
	}
}

static SProtocolData sProtocolData;

SProtocolData& getProtocolData() {
	return sProtocolData;
}

/**
 * 获取校验码
 * Get checksum code
 */
UINT16 getCheckSum(const BYTE *pData, int len) {
	int sum = 0;
	for (int i = 2; i < len; ++i) {
		sum += pData[i];
	}

#ifdef CHECK_SUM_ALL
	return sum;
#else
	return (~sum + 1);
#endif
}

/**
 * 解析每一帧数据
 * Parse each frame of data
 */
static void procParse(const BYTE *pData, UINT len) {

	Protocol_on_flag = true;

	if(pData[3] == 0x02)// CmdID
	{
		if(pData[4] == 0x01)//state
		{
			switch (pData[5])
			{
				case 0x01://网络状态
					sProtocolData.netState = pData[6];
					break;

				case 0x02://网络时间
					if(MAKEWORD(pData[7],pData[6])<1970)
						sProtocolData.year = 1970;
					else
						sProtocolData.year = MAKEWORD(pData[7],pData[6]);

					sProtocolData.month = pData[8];
					sProtocolData.day = pData[9];
					sProtocolData.hour = pData[10];
					sProtocolData.minute = pData[11];
					sProtocolData.second = pData[12];
					break;

				case 0x03://温湿度
					sProtocolData.temperature = pData[6];
					sProtocolData.Humidity = pData[7];
					break;

				case 0x04://天气状况
					sProtocolData.Weather = pData[6];
					break;

				case 0x05://当前连接的设备数量
					sProtocolData.DevNumT = pData[6];
					break;

				case 0x06://电池电量
					sProtocolData.BatteryCapacity = pData[6];
					break;

				case 0x07://出厂状态
					sProtocolData.FactoryState = pData[6];
					break;

				case 0x08://硬件版本信息
					sProtocolData.HardwareVertion[0] = pData[6];
					sProtocolData.HardwareVertion[1] = pData[7];
					sProtocolData.HardwareVertion[2] = pData[8];
					sProtocolData.HardwareVertion[3] = pData[9];
					sProtocolData.HardwareVertion[4] = pData[10];
					sProtocolData.HardwareVertion[5] = pData[11];
					break;
				default:
					break;
			}
		}
		else if(pData[4] == 0x02)//获取N号设备ID
		{
			sProtocolData.DevStyle[pData[5]][0] = pData[6];
			sProtocolData.DevStyle[pData[5]][1] = pData[7];
			sProtocolData.DevStyle[pData[5]][2] = pData[8];
			sProtocolData.DevStyle[pData[5]][3] = pData[9];

			sProtocolData.DevN[pData[5]][0] = pData[10];
			sProtocolData.DevN[pData[5]][1] = pData[11];
			sProtocolData.DevN[pData[5]][2] = pData[12];
			sProtocolData.DevN[pData[5]][3] = pData[13];

			DevStyle[pData[5]][0] = pData[6];
			DevStyle[pData[5]][1] = pData[7];
			DevStyle[pData[5]][2] = pData[8];
			DevStyle[pData[5]][3] = pData[9];

			DevN[pData[5]][0] = pData[10];
			DevN[pData[5]][1] = pData[11];
			DevN[pData[5]][2] = pData[12];
			DevN[pData[5]][3] = pData[13];
		}
	}
	else if(pData[3] == 0x14)//添加设备
	{
		sProtocolData.DevState = pData[4];
	}
	// 通知协议数据更新
  // Notify protocol data update
	notifyProtocolDataUpdate(sProtocolData);

	Protocol_on_flag = false;
}

/**
 * 功能：解析协议 
 * Function: Parse protocol
 * 参数：pData 协议数据，len 数据长度
 * Parameters: pData - protocol data, len - data length
 * 返回值：实际解析协议的长度
 * Return value: the length of the actual resolution protocol
 */
int parseProtocol(const BYTE *pData, UINT len) {
	UINT remainLen = len;	// 剩余数据长度 Remaining data length
	UINT dataLen;	// 数据包长度 Packet length
 	UINT frameLen;	// 帧长度 Frame length

	/**
	 * 以下部分需要根据协议格式进行相应的修改，解析出每一帧的数据
   * The following parts need to be modified according to the protocol format to parse out the data of each frame
	 */
	while (remainLen >= DATA_PACKAGE_MIN_LEN) {
		// 找到一帧数据的数据头
    // Find the data header of a frame of data
		while ((remainLen >= 2) && ((pData[0] != CMD_HEAD1) || (pData[1] != CMD_HEAD2))) {
			pData++;
			remainLen--;
			continue;
		}

		if (remainLen < DATA_PACKAGE_MIN_LEN) {
			break;
		}

		dataLen = pData[2];
		frameLen = dataLen + SynchFrame_LEN + DataLen_LEN;
		if (frameLen > remainLen) {
			// 数据内容不全
      // Incomplete data packet
			break;
		}

		// 打印一帧数据，需要时在CommDef.h文件中打开DEBUG_PRO_DATA宏
    // To print a data of frame, open the DEBUG_PRO_DATA macro in the CommDef.h file when needed
#ifdef DEBUG_PRO_DATA
		for (UINT i = 0; i < frameLen; ++i) {
			LOGD("%x ", pData[i]);
		}
		LOGD("\n");
#endif

		// 支持checksum校验，需要时在CommDef.h文件中打开PRO_SUPPORT_CHECK_SUM宏
    // Support checksum verification, open the PRO_SUPPORT_CHECK_SUM macro in CommDef.h file when needed
#ifdef PRO_SUPPORT_CHECK_SUM
		// 检测校验码 Checksum
		//if (getCheckSum(pData, dataLen -2 + 3) == MAKEWORD(pData[frameLen - 1],pData[frameLen - 2])) {
		if (getCheckSum(pData, frameLen - CheckSum_LEN) == MAKEWORD(pData[frameLen - 1],pData[frameLen - 2])) {
			// 解析一帧数据
      // Parse a data of frame
			procParse(pData, frameLen);
		} else {
			LOGE("CheckSum error!!!!!! %x\n",getCheckSum(pData, frameLen - CheckSum_LEN));
		}
#else
		// 解析一帧数据
    // Parse a data of frame
		procParse(pData, frameLen);
#endif

		pData += frameLen;
		remainLen -= frameLen;
	}

	return len - remainLen;
}
