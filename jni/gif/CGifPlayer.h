/*
 * CGifPlayer.h
 *
 *  Created on: 2021年9月4日
 *      Author: Admin
 */

#ifndef JNI_LOGIC_CGIFPLAYER_H_
#define JNI_LOGIC_CGIFPLAYER_H_
#include "app/Activity.h"
#include "entry/EasyUIContext.h"

#include "uart/ProtocolData.h"
#include "uart/ProtocolParser.h"

#include "utils/Log.h"
#include "control/ZKDigitalClock.h"
#include "control/ZKButton.h"
#include "control/ZKCircleBar.h"
#include "control/ZKDiagram.h"
#include "control/ZKListView.h"
#include "control/ZKPointer.h"
#include "control/ZKQRCode.h"
#include "control/ZKTextView.h"
#include "control/ZKSeekBar.h"
#include "control/ZKEditText.h"
#include "control/ZKVideoView.h"
#include "window/ZKSlideWindow.h"

#include "system/Thread.h"
#include "gif_lib.h"


	/**return 1: stop play, 0:continux**/
typedef bool (*on_playend)();

/***
 * 仅用于Z21的定义，用于setbackgroundbmp的结构体
 */

typedef struct _bitmap_t {
    uint8_t   bmType;
    uint8_t   bmBitsPerPixel;
    uint8_t   bmBytesPerPixel;
    uint8_t   bmAlpha;
    uint32_t  bmColorKey;
    uint32_t  bmWidth;
    uint32_t  bmHeight;
    uint32_t  bmPitch;
    uint8_t*  bmBits;
    uint8_t*  bmAlphaMask;
    uint32_t  bmAlphaPitch;
    uint64_t  bmPhyAddr;
} bitmap_t;

class CGifPlayer:public Thread{
public:
	CGifPlayer(ZKBase* view);
	virtual ~CGifPlayer();
	bool play(const char* filename,bool onshort = 0);
	void stop();
	void setPlayendCallback(on_playend callback);
	bool threadLoop();
private:
	bool mOneShort;
	string playfile;
	int playstatus;
	string playFileName;
	int trans_color;
	bitmap_t bitmap;
	ZKTextView* mTextView;
	on_playend playcallback;

};


#endif /* JNI_LOGIC_CGIFPLAYER_H_ */
