#pragma once
#include "uart/ProtocolSender.h"
#include <time.h>
#include "event/EventContext.h"
#include "gif/CGifPlayer.h"
#include "storage/StoragePreferences.h"
#include "uart/ProtocolSender.h"
#include "utils/TimeHelper.h"
#include "utils/BrightnessHelper.h"

BYTE backlight_state = 1;
int screenoff_count = 0;

BYTE DevStyle[NAME_SIZE][4];
BYTE DevN[NAME_SIZE][4];
BYTE DevNumNow = 1;
BYTE DevNumT = 0;
BYTE DevAddingState = 4;//设备添加状态
BYTE DevAddState = 0;
BYTE DevAdd_count = 0;
BYTE FactoryState = 0;
BYTE NetSettingState = 4;//网络设置状态
BYTE NetState = 0;
BYTE NetConnect_count = 0;
BYTE Setting_state = 0;
bool BackKEY_count_flag = false;
BYTE BackKEY_count = 0;
bool E_KEYCODE_password = true;//旋钮输入密码


bool Protocol_on_flag = false;//协议在通讯标志
BYTE stateMM[] = { 0x01,0x01, 0x00,0x00,0x00,0x00,0x00,0x00 };//发送开锁密码
BYTE stateDevNumT[] = { 0x01,0x05 };//获取当前设备连接数
BYTE stateDevID[] = { 0x02,0x01 };//获取设备ID
BYTE stateFactory[] = { 0x01,0x07 };//获取出厂状态
BYTE stateNetS[] = { 0x01,0x01 };//获取网络状态
BYTE stateTime[] = { 0x01,0x02 };//获取网络时间
BYTE stateTH[] = { 0x01,0x03 };//获取温湿度

BYTE stateAddDEV[] = { 0x01 };//开始添加设备

CGifPlayer* gif;
CGifPlayer* gif1;
CGifPlayer* gif2;
CGifPlayer* gif3;

static BYTE count = 1;
//uint32_t key_count = 0;

#define TIMER_HANDLE 1
#define NET_HANDLE 2

#define TAB_SIZE 12
int COMMON_COUNT = 0;

static std::string sContentStr;

typedef struct {
	int id;
	char ch;
} SNumKeypadInfo;

static const SNumKeypadInfo sNumKeypadInfoTab[] = {
	0, 0,
	ID_MAIN_NUM1, '1',
	ID_MAIN_NUM2, '2',
	ID_MAIN_NUM3, '3',
	ID_MAIN_NUM4, '4',
	ID_MAIN_NUM5, '5',
	ID_MAIN_NUM6, '6',
	ID_MAIN_NUM7, '7',
	ID_MAIN_NUM8, '8',
	ID_MAIN_NUM9, '9',
	ID_MAIN_NUM0, '0',
	ID_MAIN_Delete, '*',
	ID_MAIN_Confirm, '#',
	0, 0
};

static void addOneChar(char ch) {
	if(sContentStr.length()<6)
	{
		sContentStr += ch;
		mTEXTVIEW_CONTENTPtr->setText(sContentStr);
	}
}

static void delOneChar(uint32_t pos) {
//	if (!sContentStr.empty()) {
//		sContentStr.erase(sContentStr.length() - 1, 1);
//		mTEXTVIEW_CONTENTPtr->setText(sContentStr);
//	}

	if (sContentStr.empty() || (pos <= 0) || (pos > sContentStr.size())) {
		return;
	}

	int size = 1;
	pos--;
	if (sContentStr[pos] & 0x80) {
		while ((pos > 0) && !(sContentStr[pos] & 0x40)) {
			size++;
			pos--;
		}
	}
	sContentStr.erase(pos, size);

	mTEXTVIEW_CONTENTPtr->setText(sContentStr);
}


static void oneButtonSearchNUM(ZKButton *pButton) {
	for (int i = 1; sNumKeypadInfoTab[i].id != 0; i++)
	{
		if (sNumKeypadInfoTab[i].id == pButton->getID())
		{
			addOneChar(sNumKeypadInfoTab[i].ch);
			break;
		}
	}
}

static void reshowAllKey() {
	for (int i = 1; sNumKeypadInfoTab[i].id != 0; ++i) {
		ZKButton *pButton = (ZKButton *) mActivityPtr->findControlByID(sNumKeypadInfoTab[i].id);
		if(COMMON_COUNT==i)
			pButton->setSelected(1);
		else
			pButton->setSelected(0);
	}
}

bool gifplaycallback(){
	return 0;
}

static void updateUI_time() {    //定时日期函数
	char timeStr[20];
	struct tm *t = TimeHelper::getDateTime();

	sprintf(timeStr, "%02d:%02d:%02d", t->tm_hour,t->tm_min,t->tm_sec);
	mTextTimePtr->setText(timeStr); // 注意修改控件名称

	sprintf(timeStr, "%d年%02d月%02d日", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday);
	mTextDatePtr->setText(timeStr); // 注意修改控件名称

	static const char *day[] = { "日", "一", "二", "三", "四", "五", "六" };
	sprintf(timeStr, "星期%s", day[t->tm_wday]);
	mTextWeekPtr->setText(timeStr); // 注意修改控件名称
}

/*static void updateDateEditText() {//更新日期
	struct tm *t = TimeHelper::getDateTime();
	mYearEdittextPtr->setText(t->tm_year + 1900);
	mMonthEdittextPtr->setText(t->tm_mon + 1);
	mDayEdittextPtr->setText(t->tm_mday);
}
*/
static S_ACTIVITY_TIMEER REGISTER_ACTIVITY_TIMER_TAB[] = {
		{0,  1000}, //定时器id=0, 时间间隔1秒
		{1,  7000},
		//{2,  2000},
};

static void ClearMM(void)//清除密码
{
    sContentStr.clear();
    mTEXTVIEW_CONTENTPtr->setText(sContentStr);
    sContentStr[2] = 0;
    sContentStr[3] = 0;
    sContentStr[4] = 0;
    sContentStr[5] = 0;
    sContentStr[0] = 0;
    sContentStr[1] = 0;

    COMMON_COUNT = 0;
	for (int i = 1; sNumKeypadInfoTab[i].id != 0; ++i)
	{
		ZKButton *pButton = (ZKButton *) mActivityPtr->findControlByID(sNumKeypadInfoTab[i].id);
		pButton->setSelected(0);
	}
}

static void ShowPic(int num)
{
	char temp[10] = {0};

    mWindowGIFPtr->hideWnd();//开机动画
    mWindowHomePtr->hideWnd();//主页
    mWindowDrawerPtr->hideWnd();//保险箱+设备号
    mWindowPasswordPtr->hideWnd();//输入密码

    mWindowQRCodePtr->hideWnd();//二维码
    mWindowlianjiePtr->hideWnd();//WIFI连接状态3
    mWindowaugmentPtr->hideWnd();//添加设备4
    //mkaidongtaiPtr->hideWnd();//开锁动态
    mshengyinPtr->hideWnd();//声音设置
    mliangduPtr->hideWnd();//亮度设置
    msetiaoPtr->hideWnd();//色调设置
    mjiluPtr->hideWnd();//全部记录
    ClearMM();

	switch (num)
	{
	case 0:
		mWindowQRCodePtr->showWnd();
		break;
	case 1:
		mWindowHomePtr->showWnd();
		break;
    case 2:
    	LOGD("!!!!!!!!!!!! DevNumNow -- %d \n",DevNumNow);
    	if(DevStyle[DevNumNow][0] == 0x53)
    		snprintf(temp, sizeof(temp), "平移款");
    	else if(DevStyle[DevNumNow][0] == 0x50)
    		snprintf(temp, sizeof(temp), "密码款");
    	else if(DevStyle[DevNumNow][0] == 0x4D)
    		snprintf(temp, sizeof(temp), "床头款");

    	mTextViewDevNamePtr->setText(temp);

    	if(DevNumNow>=3)
    	{
        	mTextView32Ptr->setVisible(true);
        	mTextViewXPtr->setVisible(true);
        	snprintf(temp, sizeof(temp), "%d",DevNumNow-1);
        	mTextViewXPtr->setText(temp);
    	}
    	else
    	{
			mTextView32Ptr->setVisible(false);
			mTextViewXPtr->setVisible(false);
    	}

    	snprintf(temp, sizeof(temp), "%d",DevNumNow);
    	mTextViewCPtr->setText(temp);

    	if(DevNumT>DevNumNow)
    	{
        	mTextView31Ptr->setVisible(true);
        	mTextViewBPtr->setVisible(true);
			snprintf(temp, sizeof(temp), "%d",DevNumNow+1);
			mTextViewBPtr->setText(temp);
    	}
    	else
    	{
        	mTextView31Ptr->setVisible(false);
        	mTextViewBPtr->setVisible(false);
    	}

    	mWindowDrawerPtr->showWnd();
		break;
    case 3:
    	E_KEYCODE_password = true;
    	mTextViewEnter1Ptr->setVisible(true);
    	mButton21Ptr->setVisible(true);
    	mTextViewChange1Ptr->setVisible(false);
    	mTextViewChange2Ptr->setVisible(false);
    	mButtonChange3Ptr->setVisible(false);
		mWindowPasswordPtr->showWnd();
		break;
    case 4:
    	mjiluPtr->showWnd();
		break;
	case 5:

		break;
    case 6:
    	mWindowlianjiePtr->showWnd();
		break;
    case 7:
    	mWindowaugmentPtr->showWnd();
		break;
    case 8:
    	Setting_state = 1;
    	mTextViewEnter1Ptr->setVisible(false);
    	mButton21Ptr->setVisible(false);
    	mTextViewChange1Ptr->setVisible(true);
    	mTextViewChange2Ptr->setVisible(true);
    	mButtonChange3Ptr->setVisible(true);
    	mWindowPasswordPtr->showWnd();

    	E_KEYCODE_password = false;
		break;
	case 9:
		Setting_state = 2;
		mshengyinPtr->showWnd();
		break;
    case 10:
    	Setting_state = 3;
    	mliangduPtr->showWnd();
		break;
    case 11:
    	Setting_state = 4;
    	msetiaoPtr->showWnd();
		break;
    case 12:

		break;

	default:
		break;
	}
}

static void ShowWeather(int num)
{
	mTextViewW1Ptr->setVisible(false);
	mTextViewW2Ptr->setVisible(false);
	mTextViewW3Ptr->setVisible(false);
	mTextViewW4Ptr->setVisible(false);
	mTextViewW6Ptr->setVisible(false);
	mTextViewW7Ptr->setVisible(false);
	mTextViewW8Ptr->setVisible(false);
	mTextViewW9Ptr->setVisible(false);
	mTextViewWAPtr->setVisible(false);
	mTextViewWBPtr->setVisible(false);
	mTextViewWEPtr->setVisible(false);
	mTextViewWFPtr->setVisible(false);

	switch (num)
	{
	case 1:
		mTextViewW1Ptr->setVisible(true);
		break;
     case 2:
    	mTextViewW2Ptr->setVisible(true);
		break;
    case 3:
    	mTextViewW3Ptr->setVisible(true);
		break;
    case 4:
    	mTextViewW4Ptr->setVisible(true);
		break;
	case 5:

		break;
    case 6:
    	mTextViewW6Ptr->setVisible(true);
		break;
    case 7:
    	mTextViewW7Ptr->setVisible(true);
		break;
    case 8:
    	mTextViewW8Ptr->setVisible(true);
		break;
	case 9:
		mTextViewW9Ptr->setVisible(true);
		break;
    case 10:
    	mTextViewWAPtr->setVisible(true);
		break;
    case 11:
    	mTextViewWBPtr->setVisible(true);
		break;
    case 12:

		break;
    case 13:

		break;
    case 14:
    	mTextViewWEPtr->setVisible(true);
		break;
    case 15:
    	mTextViewWFPtr->setVisible(true);
		break;
    case 16:

		break;
	default:
		break;
	}
}

bool show_timeout_flag = true;
static void _on_key_event_cb(int keyCode, int keyStatus) {
	screenoff_count = 0;
	if(backlight_state == 0)
	{
		BRIGHTNESSHELPER->backlightOn();
		backlight_state = 1;
	}
	switch (keyCode) {
	case E_KEYCODE_CLOCKWISE://逆时针
		if(mWindowPasswordPtr->isWndShow()&&E_KEYCODE_password)
		{
			COMMON_COUNT--;
			if(COMMON_COUNT < 1)
				COMMON_COUNT = TAB_SIZE;

			reshowAllKey();
		}
		else
		{
			if(Setting_state==0&&FactoryState==0x01)
			{
				//count--;
				//if(count < 1)
				//	count = 2;
				if(count==1)
					count = 2;
				else if(count==2)
					count = 1;

				ShowPic(count);
			}
			else
			{
				if(Setting_state==1)
					ShowPic(11);
				else if(Setting_state==2)
					ShowPic(8);
				else if(Setting_state==3)
					ShowPic(9);
				else if(Setting_state==4)
					ShowPic(10);
			}
		}
		break;

	case E_KEYCODE_ANTI_CLOCKWISE: //顺时针
		if(mWindowPasswordPtr->isWndShow()&&E_KEYCODE_password)
		{
			COMMON_COUNT++;
			if(COMMON_COUNT > TAB_SIZE)
				COMMON_COUNT = 1;

			reshowAllKey();
		}
		else
		{
			if(Setting_state==0&&FactoryState==0x01)
			{
				//count++;
				//if(count > 2)
				//	count = 1;

				if(count==1)
					count = 2;
				else if(count==2)
					count = 1;

				ShowPic(count);
			}
			else
			{
				if(Setting_state==1)
					ShowPic(9);
				else if(Setting_state==2)
					ShowPic(10);
				else if(Setting_state==3)
					ShowPic(11);
				else if(Setting_state==4)
					ShowPic(8);
			}
		}
		break;

	case 2://按键
		//LOGD("NNNNNNNNNNNNNNNNNNNNNNN\n");
		if(mWindowPasswordPtr->isWndShow())
		{
			if(COMMON_COUNT==12)
			{
			    stateMM[2] = sContentStr[0];
			    stateMM[3] = sContentStr[1];
			    stateMM[4] = sContentStr[2];
			    stateMM[5] = sContentStr[3];
			    stateMM[6] = sContentStr[4];
			    stateMM[7] = sContentStr[5];
			    sendProtocol(0x19, stateMM, 8);
			}
			else if(COMMON_COUNT==11)
			{
				delOneChar(sContentStr.size());
			}
			else
			{
				char str = NULL;
				str = sNumKeypadInfoTab[COMMON_COUNT].ch;
				addOneChar(str);
			}
		}

		if(Setting_state!=0)
		{
			BackKEY_count_flag = true;
		}

		break;

	case 3://按键
		//LOGD("UUUUUUUUUUUUUUUUUUUUUUU\n");
		BackKEY_count_flag = false;
		if(BackKEY_count>5)
		{
			Setting_state = 0;
			ShowPic(2);
		}
		else
			BackKEY_count = 0;
		break;
	}
}

static void onUI_init(){
	event::add_key_event_cb(_on_key_event_cb);//仿
	event::start();//仿
	gif = new CGifPlayer(mTextViewgifPtr);
	gif->play("/res/ui/0888.gif",0);
	gif1 = new CGifPlayer(mTextViewgif1Ptr);
	gif1->play("/res/ui/0600.gif",0);
	gif2 = new CGifPlayer(mTextViewgif2Ptr);
	gif2->play("/res/ui/0600.gif",0);
	gif3 = new CGifPlayer(mTextViewgif3Ptr);
	gif3->play("/res/ui/0730.gif",0);
	mActivityPtr->registerUserTimer(TIMER_HANDLE, 7500);//时转

	//gif->play("/mnt/extsd/ui/ 0888.gif",0);//播放
	//gif->setPlayendCallback(gifplaycallback);//回调
	//mActivityPtr->resetUserTimer(TIMER_HANDLE, 3000);//停

	//if(!Protocol_on_flag)//不与通讯协议冲突+
	sendProtocol(0x01, stateDevNumT, 2);


	sendProtocol(0x01, stateFactory, 2);
	sendProtocol(0x01, stateTime, 2);
	sendProtocol(0x01, stateTH, 2);
}



static void onUI_intent(const Intent *intentPtr) {
    if (intentPtr != NULL) {
        //TODO
    }
}

/*
 * 当界面显示时触发
 */
static void onUI_show() {
	//gif1 = new CGifPlayer(mTextViewgifPtr);//播放
	//gif1->play("/res/ui/0600.gif",0);//播放
}

/*
 * 当界面隐藏时触发
 */
static void onUI_hide() {

}

/*
 * 当界面完全退出时触发
 */
static void onUI_quit() {
	delete gif;
}

/**
 * 串口数据回调接口
 */
static void onProtocolDataUpdate(const SProtocolData &data) {
	char temp[50] = {0};
	snprintf(temp, sizeof(temp), "%d℃", data.temperature);
	mTemperaturePtr->setText(temp);

	snprintf(temp, sizeof(temp), "%4d-%2d-%2d %2d:%2d:%2d", data.year,data.month,data.day,data.hour,data.minute,data.second);
	TimeHelper::setDateTime(temp);

	DevNumT = data.DevNumT;
	DevAddState = data.DevState;
	FactoryState = data.FactoryState;
	NetState = data.netState;

	ShowWeather(data.Weather);//显示天气图标
}

/**
 * 定时器触发函数
 * 不建议在此函数中写耗时操作，否则将影响UI刷新
 * 参数： id
 *         当前所触发定时器的id，与注册时的id相同
 * 返回值: true
 *             继续运行当前定时器
 *         false
 *             停止运行当前定时器
 */
static bool onUI_Timer(int id){
	switch (id)
	{
	case TIMER_HANDLE://延时进入
		for(int i = 0;i < DevNumT;i++)
		{
			stateDevID[1] = i+1;
			sendProtocol(0x01, stateDevID, 2);
		}

		if(FactoryState==0x01)
			ShowPic(1);//主页
		else
		{
			ShowPic(0);//二维码
			NetSettingState = 1;
		}
	    return false;
		break;

	case 0:
		if(BackKEY_count_flag)
			BackKEY_count++;

		screenoff_count++;
		if(screenoff_count >= 20)
		{
			BRIGHTNESSHELPER->backlightOff();
			backlight_state = 0;
			if(NetState<=1&&FactoryState==0x00)
				ShowPic(0);//二维码
			else
				ShowPic(1);//主页
		}

		updateUI_time();

/////////////////////网络处理/////////////////////
		if(NetSettingState==1)
		{
			sendProtocol(0x01, stateNetS, 2);
			if(NetState==0x01)
			{
				mWindowlianPtr->hideWnd();
				mWindowchenggongPtr->hideWnd();
				mWindowshibaiPtr->showWnd();

				NetSettingState = 2;
			}
			else if(NetState==0x02)
			{
				ShowPic(6);
				mWindowlianPtr->showWnd();
				mWindowchenggongPtr->hideWnd();
				mWindowshibaiPtr->hideWnd();

				NetConnect_count++;
				screenoff_count = 0;
				if(NetConnect_count>60)
				{
					mWindowlianPtr->hideWnd();
					mWindowchenggongPtr->hideWnd();
					mWindowshibaiPtr->showWnd();

					NetSettingState = 2;
				}
			}
			else if(NetState==0x03)
			{
				mWindowlianPtr->hideWnd();
				mWindowchenggongPtr->showWnd();
				mWindowshibaiPtr->hideWnd();

				NetSettingState = 2;
				mWIFIPtr->setVisible(true);
			}
		}
		else if(NetSettingState == 2)
			NetSettingState = 3;
		else if(NetSettingState == 3)
		{
			if(NetState<=1)
				ShowPic(0);//二维码
			else
				ShowPic(1);//主页
			NetSettingState = 4;
		}
/////////////////////网络处理/////////////////////

/////////////////////添加设备处理/////////////////////
		if(DevAddingState==1)
		{
			sendProtocol(0x13, stateAddDEV, 1);
			if(DevAddState==0x00)
			{
				mtianjiashebeiPtr->hideWnd();
				mtianjiazhongPtr->hideWnd();
				mtianjiachenggongPtr->hideWnd();
				mtianjiashibaiPtr->showWnd();

				DevAddingState = 2;
			}
			else if(DevAddState==0x01)
			{
				mtianjiashebeiPtr->hideWnd();
				mtianjiazhongPtr->showWnd();
				mtianjiachenggongPtr->hideWnd();
				mtianjiashibaiPtr->hideWnd();

				DevAdd_count++;
				screenoff_count = 0;
				if(DevAdd_count>50)//添加失败
				{
					DevAdd_count = 0;

					mtianjiashebeiPtr->hideWnd();
					mtianjiazhongPtr->hideWnd();
					mtianjiachenggongPtr->hideWnd();
					mtianjiashibaiPtr->showWnd();

					DevAddingState = 2;
				}
			}
			else if(DevAddState==0x02)//添加成功
			{
				mtianjiashebeiPtr->hideWnd();
				mtianjiazhongPtr->hideWnd();
				mtianjiachenggongPtr->showWnd();
				mtianjiashibaiPtr->hideWnd();

				DevAddingState = 2;
			}
		}
		else if(DevAddingState == 2)
			DevAddingState = 3;
		else if(DevAddingState == 3)
		{
			mtianjiashebeiPtr->showWnd();
			mtianjiazhongPtr->hideWnd();
			mtianjiachenggongPtr->hideWnd();
			mtianjiashibaiPtr->hideWnd();

			ShowPic(2);
			DevAddingState = 4;
		}
/////////////////////添加设备处理/////////////////////
		break;

	default:
		break;
	}

    return true;
}

/**
 * 有新的触摸事件时触发
 * 参数：ev
 *         新的触摸事件
 * 返回值：true
 *            表示该触摸事件在此被拦截，系统不再将此触摸事件传递到控件上
 *         false
 *            触摸事件将继续传递到控件上
 */
static bool onmainActivityTouchEvent(const MotionEvent &ev) {
	screenoff_count = 0;
    switch (ev.mActionStatus) {
		case MotionEvent::E_ACTION_DOWN://触摸按下
			//LOGD("时刻 = %ld 坐标  x = %d, y = %d", ev.mEventTime, ev.mX, ev.mY);
			break;
		case MotionEvent::E_ACTION_MOVE://触摸滑动
			break;
		case MotionEvent::E_ACTION_UP:  //触摸抬起
			if(backlight_state == 0)
			{
				screenoff_count = 0;
				BRIGHTNESSHELPER->backlightOn();
				backlight_state = 1;
			}
			else
			{
				if(mWindowHomePtr->isWndShow())
				{
					if(DevNumT==0)
						ShowPic(7);//添加设备
					else
						ShowPic(2);
				}
			}
			break;
		default:
			break;
	}
	return false;
}
static bool onButtonClick_Button1(ZKButton *pButton) {
    LOGD(" ButtonClick Button1 !!!\n");
    stateMM[1] = 0x02;//嵌入式
    ShowPic(4);
    return false;
}

static bool onButtonClick_Button2(ZKButton *pButton) {
    LOGD(" ButtonClick Button2 !!!\n");
	ShowPic(3);
    return false;
}

static bool onButtonClick_Button3(ZKButton *pButton) {
    LOGD(" ButtonClick Button3 !!!\n");
    if(!mWindowHomePtr->isWndShow())
    	ShowPic(3);
    return false;
}

static bool onButtonClick_Button4(ZKButton *pButton) {
    LOGD(" ButtonClick Button4 !!!\n");
    stateMM[1] = 0x01;//抽屉式
	ShowPic(4);
    return false;
}

static bool onButtonClick_Button5(ZKButton *pButton) {
    LOGD(" ButtonClick Button5 !!!\n");
    ShowPic(2);
    return false;
}

static bool onButtonClick_Button6(ZKButton *pButton) {
    LOGD(" ButtonClick Button6 !!!\n");
    ShowPic(2);
    return false;
}

static bool onButtonClick_Button7(ZKButton *pButton) {
    LOGD(" ButtonClick Button7 !!!\n");
	pButton->setSelected(!pButton->isSelected());

	sendProtocol(0x13, stateAddDEV, 1);

	mtianjiashebeiPtr->hideWnd();
	mtianjiazhongPtr->showWnd();
	mtianjiachenggongPtr->hideWnd();
	mtianjiashibaiPtr->hideWnd();

	DevAddingState = 1;
	DevAddState = 0x01;
    return false;
}



static bool onButtonClick_ButtonConfirm(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonConfirm !!!\n");
    stateMM[2] = sContentStr[0];
    stateMM[3] = sContentStr[1];
    stateMM[4] = sContentStr[2];
    stateMM[5] = sContentStr[3];
    stateMM[6] = sContentStr[4];
    stateMM[7] = sContentStr[5];
    sendProtocol(0x19, stateMM, 8);
    return false;
}

static bool onButtonClick_ButtonExit(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonExit !!!\n");
    delOneChar(sContentStr.size());
    return false;
}


static bool onButtonClick_Button20(ZKButton *pButton) {
    LOGD(" ButtonClick Button20 !!!\n");
    return false;
}
static bool onButtonClick_Button21(ZKButton *pButton) {
    LOGD(" ButtonClick Button21 !!!\n");
    if(stateMM[1] == 0x01)
    	ShowPic(2);
    //else if(stateMM[1] == 0x02)
    //	ShowPic(2);

    return false;
}
static bool onButtonClick_NUM0(ZKButton *pButton) {
    LOGD(" ButtonClick NUM0 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM1(ZKButton *pButton) {
    LOGD(" ButtonClick NUM1 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM2(ZKButton *pButton) {
    LOGD(" ButtonClick NUM2 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM3(ZKButton *pButton) {
    LOGD(" ButtonClick NUM3 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM4(ZKButton *pButton) {
    LOGD(" ButtonClick NUM4 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM5(ZKButton *pButton) {
    LOGD(" ButtonClick NUM5 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM6(ZKButton *pButton) {
    LOGD(" ButtonClick NUM6 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM7(ZKButton *pButton) {
    LOGD(" ButtonClick NUM7 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM8(ZKButton *pButton) {
    LOGD(" ButtonClick NUM8 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}

static bool onButtonClick_NUM9(ZKButton *pButton) {
    LOGD(" ButtonClick NUM9 !!!\n");
    oneButtonSearchNUM(pButton);
    return false;
}
static bool onButtonClick_Confirm(ZKButton *pButton) {
    LOGD(" ButtonClick Confirm !!!\n");
    stateMM[2] = sContentStr[0];
    stateMM[3] = sContentStr[1];
    stateMM[4] = sContentStr[2];
    stateMM[5] = sContentStr[3];
    stateMM[6] = sContentStr[4];
    stateMM[7] = sContentStr[5];
    sendProtocol(0x19, stateMM, 8);
    return false;
}

static bool onButtonClick_Delete(ZKButton *pButton) {
    LOGD(" ButtonClick Delete !!!\n");
    delOneChar(sContentStr.size());
    return false;
}
static bool onButtonClick_Button8(ZKButton *pButton) {
    LOGD(" ButtonClick Button8 !!!\n");
    return false;
}
static bool onButtonClick_Button9(ZKButton *pButton) {
    LOGD(" ButtonClick Button9 !!!\n");
    return false;
}
static bool onButtonClick_Button10(ZKButton *pButton) {
    LOGD(" ButtonClick Button10 !!!\n");
    return false;
}
static void onProgressChanged_SeekBar1(ZKSeekBar *pSeekBar, int progress) {
    //LOGD(" ProgressChanged SeekBar1 %d !!!\n", progress);
}
static void onProgressChanged_SeekBar2(ZKSeekBar *pSeekBar, int progress) {
    //LOGD(" ProgressChanged SeekBar2 %d !!!\n", progress);
}
static void onProgressChanged_SeekBar3(ZKSeekBar *pSeekBar, int progress) {
    //LOGD(" ProgressChanged SeekBar3 %d !!!\n", progress);
}
static bool onButtonClick_Button11(ZKButton *pButton) {
    LOGD(" ButtonClick Button11 !!!\n");
    return false;
}
static bool onButtonClick_Button12(ZKButton *pButton) {
    LOGD(" ButtonClick Button12 !!!\n");
    return false;
}
static bool onButtonClick_Buttonsuobiao(ZKButton *pButton) {
    LOGD(" ButtonClick Buttonsuobiao !!!\n");
    ShowPic(3);
    return false;
}

static bool onButtonClick_Buttonshezhi(ZKButton *pButton) {
    LOGD(" ButtonClick Buttonshezhi !!!\n");

    ShowPic(8);
    return false;
}
static bool onButtonClick_ButtonChange3(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonChange3 !!!\n");
    return false;
}
static bool onButtonClick_ButtonPrev(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonPrev !!!\n");
    if(DevNumNow>1)
    	DevNumNow--;
    else
    	DevNumNow = DevNumT;

    ShowPic(2);
    return false;
}

static bool onButtonClick_ButtonNext(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonNext !!!\n");
    if(DevNumNow<DevNumT)
    	DevNumNow++;
    else
    	DevNumNow = 1;

    ShowPic(2);
    return false;
}
